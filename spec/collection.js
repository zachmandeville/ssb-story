const { string, image, integer } = require('./lib/field-types')

const TANGLE = 'collection'

function Spec (type) {
  const [superType, subType] = type.split('/')

  const spec = {
    type,
    typePattern: `^${superType}\\/${subType === '*' ? '\\*' : subType}$`,
    tangle: TANGLE,
    props: {
      name: string,
      description: string,
      image: image,
      submissionDate: integer,
      recordCount: integer
    }
  }

  return spec
}

module.exports = Spec
