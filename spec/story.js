const { IntervalChecker, DateChecker } = require('./lib/date-checkers')
const { string } = require('./lib/field-types')

const TANGLE = 'story'

function Spec (type) {
  const [superType, subType] = type.split('/')

  const spec = {
    type,
    typePattern: `^${superType}\\/${subType === '*' ? '\\*' : subType}$`,
    tangle: TANGLE,
    props: {
      title: string,
      description: string,
      timeInterval: string,
      submissionDate: string,
      location: string,
      locationDescription: string,
      creator: string,
      contributionNotes: string,
      format: string,
      identifier: string,
      language: string,
      source: string,
      transcription: string,
      permission: string
    },
    hooks: {
      isRoot: [
        IntervalChecker('timeInterval'),
        DateChecker('submissionDate')
      ],
      isUpdate: [
        IntervalChecker('timeInterval'),
        DateChecker('submissionDate')
      ]
    }
  }

  return spec
}

module.exports = Spec
