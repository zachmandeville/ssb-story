const TANGLE = 'link'

function Spec (type) {
  const spec = {
    type,
    tangle: TANGLE,
    props: {
    },
    staticProps: {
      parent: { type: 'string', $ref: '#/definitions/messageId', required: true },
      child: { type: 'string', $ref: '#/definitions/messageId', required: true }
    }
  }

  return spec
}

module.exports = Spec
