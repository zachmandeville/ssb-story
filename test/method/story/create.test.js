const test = require('tape')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')

// NOTE: see get.test.js for creation with initial details

test('story/create (empty details)', async t => {
  t.plan(1)

  const server = await Server()

  const authors = {
    add: [server.id]
  }

  server.story.create('test', { authors }, (err, storyId) => {
    if (err) throw err
    t.true(isMsgId(storyId), 'accepts {} details')
    server.close()
  })
})

test('story/create (all details)', async t => {
  t.plan(1)

  const server = await Server()

  const details = {
    title: 'Graduation',
    description: 'I graduated university',
    timeInterval: '2015/2019',
    submissionDate: '2020-01-01',
    location: 'University of Waikato',
    locationDescription: 'Te Wananga O Waikato',
    // creator NOTE: requires profile id, so leaving this out
    contributionNotes: 'Wowee',
    format: '',
    identifier: '',
    language: '',
    source: '',
    transcription: '',
    permission: 'edit',
    authors: {
      add: [server.id]
    }
  }

  server.story.create('*', details, (err, storyId) => {
    if (err) throw err
    t.true(isMsgId(storyId), 'accepts all details')
    server.close()
  })
})

test('story/create type pass', async t => {
  t.plan(1)

  const server = await Server()

  const authors = {
    add: [server.id]
  }

  server.story.create('test', { authors }, (_, storyId) => {
    t.true(isMsgId(storyId), 'accepts any string type')
    server.close()
  })
})

test('story/create tombstone', async t => {
  t.plan(1)

  const server = await Server()
  const date = Date.now()

  const tombstone = {
    set: {
      date: date,
      reason: 'test'
    }
  }

  server.story.create('test', { tombstone }, (err, _) => {
    t.deepEqual(err, new Error('You cannot create a story with a tombstone'), 'doesnt accept tombstone')
    server.close()
  })
})
