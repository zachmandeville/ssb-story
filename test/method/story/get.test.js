const test = require('tape')
const Server = require('../../test-bot')
const pull = require('pull-stream')
const set = require('lodash.set')

test('story/get', async t => {
  t.plan(6)
  const server = await Server()

  function mock (type) {
    return {
      title: type + ' title',
      description: 'this is a story - ' + type,
      timeInterval: '2019/2020',
      submissionDate: '2020-05-XX',
      location: 'New Zealand',
      locationDescription: 'Some location description',
      creator: 'Some Creator',
      contributionNotes: 'Some contribution notes',
      format: 'Some format',
      identifier: 'Some identifier',
      language: 'English',
      source: 'Some source',
      transcription: 'Some transcription',
      permission: 'view',
      // recps: [server.id],
      authors: {
        add: [server.id]
      }
    }
  }

  const stories = [
    mock('Story 1'),
    mock('Story 2'),
    mock('Story 3')
  ]

  pull(
    pull.values(stories),
    pull.asyncMap((details, cb) => {
      server.story.create('test', details, cb)
    }),
    pull.collect((err, ids) => {
      if (err) throw err

      ids.forEach((id, i) => {
        server.story.get(id, (_, data) => {
          const story = stories[i]

          let expected1 = {
            key: id,
            type: 'story/test',
            originalAuthor: server.id,
            recps: null,
            states: [
              {
                key: id,

                ...story,

                authors: {
                  [server.id]: [
                    { start: i, end: null }
                  ]
                },
                type: 'test',
                tombstone: null
              }
            ],
            conflictFields: []
          }
          expected1 = {
            ...expected1,
            ...expected1.states[0],
            type: 'story/test'
          }

          t.deepEquals(expected1, data, 'gets reduced state of ' + id)

          const update = {
            title: 'NEW TITLE ' + id
          }

          if (i === 2) update.recps = [id] // test recps on the last one

          server.story.update(id, update, (err, updateId) => {
            if (err) {
              if (i === 2) {
                t.deepEqual(err, new Error('Cannot update recps field. Please check the details provided'), 'throws error on update recps for ' + id)
                return
              } else {
                throw err
              }
            }

            let expected2 = Object.assign({}, expected1)

            set(expected2, 'states[0].key', updateId)
            set(expected2, 'states[0].title', update.title)

            expected2 = {
              ...expected2,
              ...expected2.states[0],
              key: id,
              type: 'story/test'
            }

            server.story.get(id, (_, data) => {
              t.deepEqual(data, expected2, 'gets (updated) reduced state of ' + id)
              if (i === 1) server.close()
            })
          })
        })
      })
    })
  )
})
