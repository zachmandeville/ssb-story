const test = require('tape')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')

// NOTE: see get.test.js for creation with initial details

test('collection/create (empty details)', async t => {
  t.plan(1)

  const server = await Server()

  const authors = {
    add: [server.id]
  }

  server.story.collection.create('test', { authors }, (err, storyId) => {
    if (err) throw err
    t.true(isMsgId(storyId), 'accepts {} details')
    server.close()
  })
})

test('collection/create (all details)', async t => {
  t.plan(1)

  const server = await Server()

  const details = {
    name: 'Graduation',
    description: 'I graduated university',
    authors: {
      add: [server.id]
    },
    recordCount: 5
  }

  server.story.collection.create('test', details, (err, storyId) => {
    if (err) throw err
    t.true(isMsgId(storyId), 'accepts all details')
    server.close()
  })
})

test('collection/create type pass', async t => {
  t.plan(1)

  const server = await Server()

  const authors = {
    add: [server.id]
  }

  server.story.collection.create('test', { authors }, (_, storyId) => {
    t.true(isMsgId(storyId), 'accepts any string type')
    server.close()
  })
})

test('collection/create tombstone', async t => {
  t.plan(1)

  const server = await Server()
  const date = Date.now()

  const tombstone = {
    set: {
      date: date,
      reason: 'test'
    }
  }

  server.story.collection.create('test', { tombstone }, (err, _) => {
    t.deepEqual(err, new Error('You cannot create a story with a tombstone'), 'doesnt accept tombstone')
    server.close()
  })
})
