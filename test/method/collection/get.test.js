const test = require('tape')
const Server = require('../../test-bot')
const pull = require('pull-stream')
const set = require('lodash.set')

const MB = 1024 * 1024

test('collection/get', async t => {
  t.plan(6)
  const server = await Server({ recpsGuard: true })

  const date = Date.now()

  function mock (type) {
    return {
      name: `${type} Collection`,
      description: `This is a collection for ${type} based stories`,
      image: {
        blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
        unbox: '4bOkwCLbw5B9d5+H59oxDNOyIaOhfLfqOLm1MGKyTLI=.boxs',
        mimeType: 'image/png',
        size: 4 * MB,
        width: 500,
        height: 480
      },
      submissionDate: date,
      recordCount: 5,
      recps: [server.id],
      authors: {
        add: [server.id]
      }
    }
  }

  const collections = [
    mock('Waiata'),
    mock('Hui'),
    mock('Haka')
  ]

  pull(
    pull.values(collections),
    pull.asyncMap((details, cb) => {
      server.story.collection.create('*', details, cb)
    }),
    pull.collect((err, ids) => {
      if (err) throw err

      ids.forEach((id, i) => {
        server.story.collection.get(id, (_, data) => {
          const collection = collections[i]

          delete collection.recps
          delete collection.authors

          let expected1 = {
            key: id,
            type: 'collection/*',
            originalAuthor: server.id,
            recps: [server.id],
            states: [
              {
                key: id,

                ...collection,

                authors: {
                  [server.id]: [
                    { start: i, end: null }
                  ]
                },
                type: '*',
                tombstone: null
              }
            ],
            conflictFields: []
          }
          expected1 = {
            ...expected1,
            ...expected1.states[0],
            type: 'collection/*'
          }

          t.deepEquals(expected1, data, 'gets reduced state of ' + id)

          const update = {
            name: 'NEW TITLE ' + id
          }

          if (i === 2) update.recps = [id] // test recps on the last one

          server.story.collection.update(id, update, (err, updateId) => {
            if (err) {
              if (i === 2) {
                t.deepEqual(err, new Error('Cannot update recps field. Please check the details provided'), 'throws error on update recps for ' + id)
                return
              } else {
                throw err
              }
            }

            let expected2 = Object.assign({}, expected1)

            set(expected2, 'states[0].key', updateId)
            set(expected2, 'states[0].name', update.name)
            expected2 = {
              ...expected2,
              ...expected2.states[0],
              key: id,
              type: 'collection/*'
            }

            server.story.collection.get(id, (_, data) => {
              t.deepEqual(data, expected2, 'gets (updated) reduced state of ' + id)
              if (i === 1) server.close()
            })
          })
        })
      })
    })
  )
})
