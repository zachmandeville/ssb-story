const { isMsg } = require('ssb-ref')
const test = require('tape')
const { SaveCollection, SaveStory, SaveLink } = require('../../lib/helpers')
const Server = require('../../test-bot')

test('collection link/get', async t => {
  t.plan(4)
  const server = await Server({ recpsGuard: true })

  // create some test collections
  const createCollection = SaveCollection(server)
  const createStory = SaveStory(server)
  const createLink = SaveLink(server)

  const collection1Id = await createCollection({
    name: 'My Holiday 2020',
    description: 'A collection of stories from our 2020 holiday',
    recps: [server.id]
  })

  t.true(isMsg(collection1Id), 'creates collection')

  const collection2Id = await createCollection({
    name: 'Christmas 2020',
    description: 'A collection of stories from Christmas 2020',
    recps: [server.id]
  })

  t.true(isMsg(collection2Id), 'creates second collection')

  // create a test story
  const storyId = await createStory({
    title: 'Christmas Day',
    recps: [server.id]
  })

  // save a link between this story and the two collections
  const link1 = await createLink({
    parent: collection1Id,
    child: storyId,
    recps: [server.id]
  })

  const link2 = await createLink({
    parent: collection2Id,
    child: storyId,
    recps: [server.id]
  })

  // get the links by the story
  server.story.collection.getStoryLinks({ storyId }, (err, links) => {
    t.error(err, 'returns no errors')

    t.deepEqual(
      links,
      [
        {
          key: link1,
          type: 'link/collection-story',
          originalAuthor: server.id,
          recps: [server.id],
          parent: collection1Id,
          child: storyId,
          states: [{
            key: link1,
            tombstone: null,
            authors: links[0].states[0].authors, // hack
            type: 'collection-story'
          }]
        },
        {
          key: link2,
          type: 'link/collection-story',
          originalAuthor: server.id,
          recps: [server.id],
          parent: collection2Id,
          child: storyId,
          states: [{
            key: link2,
            tombstone: null,
            authors: links[1].states[0].authors, // hack
            type: 'collection-story'
          }]
        }
      ].map(updateOutput)
    )
    function updateOutput (expected) {
      return {
        ...expected,
        ...expected.states[0],
        conflictFields: [],
        key: expected.key,
        type: expected.type
      }
    }

    server.close()
  })
})
