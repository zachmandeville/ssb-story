const test = require('tape')
const Server = require('../../test-bot')
const { isMsgId } = require('ssb-ref')

// NOTE: see get.test.js for creation with initial details

test('link/create (empty details)', async t => {
  t.plan(1)

  const server = await Server()

  const authors = {
    add: [server.id]
  }

  server.story.collection.createStoryLink('test', { authors }, (err, linkId) => {
    t.deepEqual(err, new Error('data.collection is required; data.story is required'), 'returns error for missing collection + story')
    server.close()
  })
})

test('link/create (all details)', async t => {
  t.plan(1)

  const server = await Server({ recpsGuard: true })

  const collectionId = '%d5jLD/p/x741Prb1C0NPTBsuWD/2lMmR/xqPK/lZVo0=.sha256'
  const storyId = '%b1C0NPTBd5jLD/p/x741PrsuWD/2lMmR/xqPK/lZVo0=.sha256'

  const details = {
    parent: collectionId,
    child: storyId,
    authors: {
      add: [server.id]
    },
    recps: [server.id]
  }

  server.story.collection.createStoryLink('test', details, (err, collectionId) => {
    if (err) throw err
    t.true(isMsgId(collectionId), 'accepts all details')
    server.close()
  })
})

test('collection/create tombstone', async t => {
  t.plan(1)

  const server = await Server()
  const date = Date.now()

  const tombstone = {
    date: date,
    reason: 'test'
  }

  server.story.collection.createStoryLink('test', { tombstone }, (err, _) => {
    t.deepEqual(err, new Error('You cannot create a story with a tombstone'), 'doesnt accept tombstone')
    server.close()
  })
})
