const test = require('tape')
const Server = require('../../test-bot')
const pull = require('pull-stream')
const { isMsg } = require('ssb-ref')
const { SaveCollection } = require('../../lib/helpers')

test('link/get', async t => {
  t.plan(8)
  const server = await Server({ recpsGuard: true })
  const createCollection = SaveCollection(server)

  const collectionId = await createCollection({ name: 'My Holiday 2020', description: 'A collection of stories from our 2020 holiday', recps: [server.id] })
  t.true(isMsg(collectionId), 'creates collection')

  const stories = [
    { title: 'Day 1', description: 'Packed the car and headed off on Holiday' },
    { title: 'Day 2', description: 'Went exploring in the sea' },
    { title: 'Day 3', description: 'Hiking up the maunga' }
  ]

  const tombstone = {
    date: Date.now(),
    reason: 'test'
  }

  pull(
    pull.values(stories),
    pull.asyncMap((input, cb) => {
      input.authors = {
        add: [server.id]
      }
      input.recps = [server.id]
      server.story.create('test', input, cb)
    }),
    pull.collect((err, ids) => {
      t.error(err, 'creates stories without error')

      const linkIds = []

      ids.forEach((id, i) => {
        const input = {
          parent: collectionId,
          child: id,
          recps: [server.id],
          authors: {
            add: ['*']
          }
        }

        server.story.collection.createStoryLink('collection-story', input, (err, linkId) => {
          t.error(err, 'creates collection-story link without error')

          linkIds[i] = linkId

          if (i === 2) { // hacky way to continue after the last
            // get all the links back

            // tombstone the last link
            server.story.collection.updateStoryLink(linkId, { tombstone }, (err) => {
              t.error(err, 'tombstone link without errors')

              server.story.collection.getStoryLinks({ collectionId }, (err, links) => {
                t.error(err, 'gets links without errors')

                const sharedDetails = (i) => ({
                  key: linkIds[i],
                  type: 'link/collection-story',
                  originalAuthor: server.id,
                  recps: [server.id],
                  parent: collectionId,
                  child: ids[i],
                  states: [{
                    key: linkIds[i],
                    tombstone: null,
                    authors: links[i].states[0].authors,
                    type: 'collection-story'
                  }]
                })

                t.deepEqual(
                  links,
                  [
                    sharedDetails(0),
                    sharedDetails(1)
                    // ignores the third because it was tombstoned
                  ].map(updateOutput)
                )
                function updateOutput (expected) {
                  return {
                    ...expected,
                    ...expected.states[0],
                    conflictFields: [],
                    key: expected.key,
                    type: expected.type
                  }
                }

                server.close()
              })
            })
          }
        })
      })
    })
  )
})
