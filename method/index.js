const CRUT = require('ssb-crut-authors')

// type specs
const StorySpec = require('../spec/story')
const CollectionSpec = require('../spec/collection')
const LinkSpec = require('../spec/link')
const { STORY, COLLECTION, LINK } = require('../lib/constants')

module.exports = function Method (server) {
  const crutCache = {}

  function LoadCrutFromRoot (findOrCreateCrutByType) {
    return function loadCrutFromRoot (id, cb) {
      server.get({ id, private: true, meta: true }, (err, root) => {
        if (err) return cb(err)

        cb(null, findOrCreateCrutByType(root.value.content.type))
      })
    }
  }

  function FindOrCreateCrut (SpecBuilder) {
    return function findOrCreateCrut (type) {
      if (!crutCache[type]) crutCache[type] = new CRUT(server, SpecBuilder(type))

      return crutCache[type]
    }
  }

  function buildAPI (SpecBuilder, superType, opts = {}) {
    /*
      // optional:

      opts: {
        // customise function names
        create: String
        update: String
        get: String

        // a function that takes the spec
        // as a param and returns an object
        // to be added to the API
        additionalAPI: Function
      }
    */
    const findOrCreateCrut = FindOrCreateCrut(SpecBuilder)

    const {
      create = 'create',
      update = 'update',
      get = 'get',
      additionalAPI = () => {}
    } = opts

    return {
      [create]: require('./create')(server, findOrCreateCrut, superType),
      [update]: require('./update')(server, LoadCrutFromRoot(findOrCreateCrut)),
      [get]: require('./get')(server, LoadCrutFromRoot(findOrCreateCrut), superType),
      ...additionalAPI(findOrCreateCrut, superType)
    }
  }

  return {
    ...buildAPI(StorySpec, STORY),
    // link: {
    //   getCollectionLinks: require('./link/get-collection-links')
    // },
    collection: {
      ...buildAPI(CollectionSpec, COLLECTION),
      ...buildAPI(
        LinkSpec,
        LINK, {
          create: 'createStoryLink',
          get: 'getStoryLink',
          update: 'updateStoryLink',
          additionalAPI (crut, _) {
            return {
              getStoryLinks: require('./link/get-story-links')(server, crut)
            }
          }
        }
      )
    }
  }
}
