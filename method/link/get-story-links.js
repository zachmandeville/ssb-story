const { isMsg } = require('ssb-ref')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { COLLECTION_STORY_LINK } = require('../../lib/constants')

module.exports = function GetStoryLinks (server, loadCrut) {
  const getActiveLink = (linkId, cb) => {
    server.story.collection.getStoryLink(linkId, (err, link) => {
      if (err) return cb(null, null)

      if (link.states.every(state => state.tombstone)) return cb(null, null)
      // NOTE "null" link means no link / tombstoned

      cb(null, link)
    })
  }

  const crut = loadCrut(COLLECTION_STORY_LINK)

  return function getStoryLinks ({ collectionId, storyId }, cb) {
    if (!collectionId && !storyId) return cb(new Error('getStoryLinks() needs a collection or story id'))

    // build the filter based on input
    const filter = {}

    if (collectionId) {
      if (!isMsg(collectionId)) return cb(new Error('The collectionId given to getStoryLinks() is not a valid'))
      filter.parent = collectionId
    }
    if (storyId) {
      if (!isMsg(storyId)) return cb(new Error('The storyId given to getStoryLinks() is not a valid'))
      filter.child = storyId
    }

    const query = [{
      $filter: {
        dest: collectionId || storyId,
        value: {
          content: {
            type: COLLECTION_STORY_LINK,
            ...filter,
            tangles: {
              link: { root: null, previous: null }
            }
          }
        }
      }
    }]

    pull(
      server.backlinks.read({ query }),
      pull.filter(crut.spec.isRoot),
      pull.map(link => link.key),
      paraMap(getActiveLink, 4),
      pull.filter(Boolean),
      pull.collect((err, results) => {
        if (err) return cb(err)

        cb(null, results)
      })
    )
  }
}
