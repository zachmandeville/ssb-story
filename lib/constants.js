module.exports = {
  STORY: 'story',
  LINK: 'link',
  COLLECTION: 'collection',
  COLLECTION_STORY_LINK: 'link/collection-story'
}
