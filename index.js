const API = require('./method')

module.exports = {
  name: 'story',
  version: require('./package.json').version,
  manifest: {
    create: 'async',
    get: 'async',
    update: 'async',
    collection: {
      create: 'async',
      get: 'async',
      update: 'async'
    }
  },
  init: (server) => {
    if (!server.backlinks) throw new Error('ssb-story requires ssb-backlinks')
    return API(server)
  }
}
